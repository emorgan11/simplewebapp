json.array!(@user_notes) do |user_note|
  json.extract! user_note, :id, :note, :user_id
  json.url user_note_url(user_note, format: :json)
end
