class CreateUserNotes < ActiveRecord::Migration
  def change
    create_table :user_notes do |t|
      t.text :note
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
